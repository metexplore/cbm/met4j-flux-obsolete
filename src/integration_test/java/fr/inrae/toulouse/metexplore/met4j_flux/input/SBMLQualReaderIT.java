/*
 * Copyright INRAE (2020)
 *
 * contact-metexplore@inrae.fr
 *
 * This software is a computer program whose purpose is to [describe
 * functionalities and technical features of your software].
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "https://cecill.info/licences/Licence_CeCILL_V2.1-en.html".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 */

package fr.inrae.toulouse.metexplore.met4j_flux.input;

import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.CplexBind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.GLPKBind;
import fr.inrae.toulouse.metexplore.met4j_flux.interaction.RelationFactory;
import fr.inrae.toulouse.metexplore.met4j_flux.interaction.Unique;
import fr.inrae.toulouse.metexplore.met4j_flux.io.Utils;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import org.junit.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class SBMLQualReaderIT {

    static String coliFileString = "";
    static String condFileString = "";
    static String qualString = "";

    @Test
    public void test() throws Met4jSbmlReaderException, IOException {

        File file;
        try {
            file = java.nio.file.Files.createTempFile("coli", ".xml").toFile();

            coliFileString = Utils.copyProjectResource(
                    "SBMLQual/coli_core.xml", file);

            file = java.nio.file.Files.createTempFile("cond", ".txt").toFile();

            condFileString = Utils
                    .copyProjectResource(
                            "SBMLQual/conditionsFBA.txt",
                            file);

            file = java.nio.file.Files.createTempFile("qual", ".xml").toFile();

            qualString = Utils.copyProjectResource(
                    "SBMLQual/test_myb30.xml", file);

        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }

        Bind bind = null;

        String solver = "GLPK";
        if (System.getProperties().containsKey("solver")) {
            solver = System.getProperty("solver");
        }

        try {
            if (solver.equals("CPLEX")) {
                bind = new CplexBind();
            } else {
                bind = new GLPKBind();
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            fail("Solver error");
        }

        bind.loadSbmlNetwork(coliFileString);

        bind.loadConstraintsFile(condFileString);

        SBMLQualReader.loadSbmlQual(qualString, bind.getInteractionNetwork(),
                new RelationFactory());

        BioEntity entity1 = bind.getInteractionNetwork().getEntity("s_MYB30");
        BioEntity entity2 = bind.getInteractionNetwork().getEntity("s_MYB96");
        BioEntity entity3 = bind.getInteractionNetwork().getEntity("s_VLCFA");
        BioEntity entity4 = bind.getInteractionNetwork()
                .getEntity("s_Bacteria");
        BioEntity R_ACALD = bind.getInteractionNetwork()
                .getEntity("R_ACALD");

        assertTrue(bind.getInteractionNetwork().getInitialState(entity1) == 3);

        assertTrue(bind.getInteractionNetwork().getInitialState(entity2) == 1);

        assertTrue(bind.getInteractionNetwork().getInitialState(entity3) == 0);

        assertTrue(bind.getInteractionNetwork().getInitialState(R_ACALD) == 1);

        // /interactions

        assertTrue(bind.getInteractionNetwork().getTargetToInteractions()
                .get(entity1).getConditionalInteractions().get(0)
                .getCondition().getInvolvedEntities().get(0).getId()
                .equals("s_Bacteria"));

        assertTrue(((Unique) bind.getInteractionNetwork()
                .getTargetToInteractions().get(entity1)
                .getConditionalInteractions().get(0).getCondition()).getValue() == 6);

        assertTrue(bind.getInteractionNetwork().getTargetToInteractions()
                .get(entity1).getConditionalInteractions().get(0)
                .getConsequence().getEntity() == entity1);

        assertTrue(bind.getInteractionNetwork().getTargetToInteractions()
                .get(entity1).getConditionalInteractions().get(0)
                .getConsequence().getValue() == 1);

        assertTrue(bind.getInteractionNetwork().getTargetToInteractions()
                .get(entity1).getdefaultInteraction().getConsequence()
                .getValue() == 0);

        assertTrue(bind.getInteractionNetwork()
                .getConstraintFromState(entity1, 0).getLb() == 0.0);
        assertTrue(bind.getInteractionNetwork()
                .getConstraintFromState(entity1, 0).getUb() == 0.5);

        assertTrue(bind.getInteractionNetwork().getStateFromValue(entity1, 0.7) == 1);

        assertTrue(bind.getInteractionNetwork().getStateFromValue(R_ACALD, 0) == 0);
        assertTrue(bind.getInteractionNetwork().getStateFromValue(R_ACALD, 10.0) == 1);



        // System.out.println(bind.getInteractionNetwork().getTargetToInteractions()
        // .get(entity2).getConditionalInteractions().get(0));

    }

}
